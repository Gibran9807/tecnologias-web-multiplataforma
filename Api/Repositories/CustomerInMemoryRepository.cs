using System;
using System.Collections.Generic;
using System.Linq;
using Api.Repositories.Interfaces;
using Entities;
using System.Threading.Tasks;

namespace Api.Repositories
{
    public class CustomerInMemoryRepository:ICustomerRepository
    {
        public async Task<List<Customer>> GetAllAsync()
        {
            var lst = new List<Customer>();
            lst.Add(new Customer{Id = 1, Name = "Customer"});

            return await Task.FromResult(lst);
        }
        
    }
}