using Xunit;
using Entities;

namespace test.Entities;

public class Customer_test
{


    [Fact]
    public void Name_WhenSetNameValue_ReturnSameValue()
    {
        //arrange
        var expected = "Delasalle";
        var sut = new Customer();

        //act
        sut.Name = expected;
        var result = sut.Name;
        
        //assert
        Assert.Equal(expected, result);
    }
}